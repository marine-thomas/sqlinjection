<?php
$db_file = '/var/www/html/database.db';

try {
    $conn = new PDO("sqlite:$db_file");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $user = $_POST['username'];
    $pass = $_POST['password'];

    // $sql = "SELECT * FROM users WHERE username = '$user' AND password = '$pass'";
    // $stmt = $conn->query($sql);
    $sql = $conn->prepare("SELECT * FROM users WHERE username = :username AND password = :password");
    $sql->bindParam(':username', $user, PDO::PARAM_STR);
    $sql->bindParam(':password', $pass, PDO::PARAM_STR);
    $sql->execute();

    if ($sql === false) {
        echo "Error executing the query.<br>";
    } else {
        $rows = $sql->fetchAll(PDO::FETCH_ASSOC);
        $rowCount = count($rows);

        if ($rowCount > 0) {
            echo "Login successful!<br>";
            foreach ($rows as $row) {
                echo "id: " . $row["id"]. " - Username: " . $row["username"]. " - Password: " . $row["password"]. "<br>";
            }
        } else {
            echo "Incorrect username or password.";
        }
    }
} catch (PDOException $e) {
    echo "Connection error: " . $e->getMessage();
}
